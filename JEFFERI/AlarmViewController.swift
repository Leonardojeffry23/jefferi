


import UIKit
import UserNotifications

class AlarmViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    var hourinteger = 0
    var minuteinteger = 0
    var timeset = "01:01"
     var notifications = [Notification]()
    var hourstring = "01"
    var minutestring = "01"
    var hoursarray = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","00"]
    var minutesarray = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","00"]
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        2
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
           return hoursarray.count
        }
        else {
            return minutesarray.count
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return hoursarray[row]
        }
        return minutesarray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        
        if component == 0 {
            hourstring = hoursarray [row]
        }
        else {
            minutestring = minutesarray[row]
        }
        
        timeset = hourstring + ":" + minutestring

//        hourinteger  = Int(hourstring)!
//        minuteinteger = Int(minutestring)!
        
       // UserDefaults.standard.set(hourinteger, forKey: "hour")
       // UserDefaults.standard.set(minuteinteger, forKey: "minute")
        
    }

    
    
    
    @IBOutlet weak var alarmpickerView: UIPickerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        alarmpickerView.delegate = self
        alarmpickerView.dataSource = self
    
      
     
    }
    
    @IBAction func saveAlarm(_ sender: Any) {

        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert,.badge,.sound]) {
            (granted,error) in
        }
        
        let content = UNMutableNotificationContent()
        content.title = "Hey Kamu !!"
        content.body = "Saatnya untuk mulai menulis hari selanjutnya"
         

        var dateFormatter = DateFormatter()
        let myOwnDate = Date()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let currentDate = dateFormatter.string(from: myOwnDate)
        let dateTime = currentDate + " " + timeset
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
        let date = dateFormatter.date(from: dateTime)
        let currentCalendar = Calendar.current
        let components = currentCalendar.dateComponents([.hour, .minute], from: date!)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: true)

        let uuidString = UUID().uuidString
        let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: trigger)

        center.add(request) { (error) in
                   if let error = error {
                       print("Error \(error.localizedDescription)")
                   }else{
                       print("Oke")
                   }

        
    }
    
  
}
}
